/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {
    public static void main(String[] args) {
        System.out.println("Sistema: " + System.getProperty("os.name"));
        System.out.println("Numero de processadores: " + Runtime.getRuntime().availableProcessors());
        System.out.println("Memoria total: " + (Runtime.getRuntime().totalMemory()/1048576) + "MB");
        System.out.println("Memoria livre: " + (Runtime.getRuntime().freeMemory()/1048576) + "MB");
        System.out.println("Memoria maxima: " + ((Runtime.getRuntime().maxMemory())/1048576) + "MB");
        
        
    }
}
